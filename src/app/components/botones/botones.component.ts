import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-botones',
  templateUrl: './botones.component.html',
  styleUrls: ['./botones.component.css']
})
export class BotonesComponent implements OnInit {

  texto:string = 'Mensaje Informativo';
  boton1:string = 'Botón 1';
  boton2:string = 'Botón 2';
  boton3: string = 'Botón 3';
  
  number!:number;


  constructor() { }

  ngOnInit(): void {
  }
  btn1(){
    this.number = 1
  }

  btn2(){
    this.number = 2
  }

  btn3(){
    this.number = 3
  }
}
